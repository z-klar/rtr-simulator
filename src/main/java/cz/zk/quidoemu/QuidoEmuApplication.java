package cz.zk.quidoemu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuidoEmuApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuidoEmuApplication.class, args);
	}

}
