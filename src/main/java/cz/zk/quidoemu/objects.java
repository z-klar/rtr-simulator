package cz.zk.quidoemu;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name = "objects")
@ApiModel(description = "All details about the Outputs. ")
public class objects {

    @ApiModelProperty(notes = "The Output ID")
    private int id;

    @ApiModelProperty(notes = "Current value of the Output")
    private int value;

    public objects () {

    }

    public objects(int id, int value) {
        this.id = id;
        this.value = value;
    }

    @Column(name = "id", nullable = false)
    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    @Column(name = "value", nullable = false)
    public int getvalue() {
        return value;
    }

    public void setvalue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Output [id=" + id + ", value=" + value + "]";
    }

}
